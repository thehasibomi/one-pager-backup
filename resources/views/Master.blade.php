<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>
        @yield('title')
    </title>

    <!-- Foundation CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/foundation.min.css') }}">
    <!-- Custome CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">

    @yield('css')
</head>
<body>
    <div class="off-canvas-wrapper">
        <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
            <!-- title-bar -->
            <div class="title-bar">
                <div class="title-bar-left">
                    <button class="menu-icon" type="button" data-open="offCanvasLeft"></button>
                    <span class="title-bar-title">One pager Backup</span>
                </div>
            </div>

            <!-- left menu -->
            <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
                @include('Partials.Menu')
            </div>

            <!-- original conten goes in this container -->
            <div class="off-canvas-content" data-off-canvas-content>
                @yield('content')
            </div>
        <!-- closing wrappers -->
        </div>
    </div>

    <script src="{{ asset('assets/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/what-input.min.js') }}"></script>
    <script src="{{ asset('assets/js/foundation.min.js') }}"></script>
    <script>
        $(document).foundation();
    </script>

    @yield('js')
</body>
</html>