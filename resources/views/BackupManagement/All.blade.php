@extends('Master')

@section('title')
    Backup Management
@stop

@section('content')
    <div class="row">
        <div class="large-12 column">
            <table class="hover stack scroll">
                <thead>
                    <tr>
                        <th>Created at</th>
                        <th>Path</th>
                        <th>File size</th>
                        <th>Is Synced</th>
                        <th>Is Deleted</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($backups as $backup)
                        <tr>
                            <td>{{ $backup->created_at }}</td>
                            <td>
                                @if($backup->is_completed == 1)
                                    <a href="{{ url('backup-management/view/' . $backup->id) }}">{{ $backup->path_to_backup }}</a>
                                @else
                                    In progress
                                @endif
                            </td>
                            <td>{{ $backup->file_size }}</td>
                            <td>{{ $backup->is_synced }}</td>
                            <td>{{ $backup->is_deleted }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div> <!-- div.large-12.column -->
    </div> <!-- div.row -->
@stop