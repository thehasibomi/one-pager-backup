@extends('Master')

@section('title')
    Backup Management
@stop

@section('content')
    <div class="row">
        <div class="large-12 column">
            <div class="callout large">
                <p><strong>Created at:</strong> {{ $backup->created_at }}</p>
                <p><strong>Path:</strong> {{ $backup->path_to_backup }}</p>
                <p><strong>File size:</strong> {{ $backup->file_size }}</p>
                <p><strong>Output text:</strong> {{ $backup->output_text }}</p>
                <p><strong>Error text:</strong> {{ $backup->error_text }}</p>
            </div>
        </div> <!-- div.large-12.column -->
    </div> <!-- div.row -->
@stop