<ul class="vertical menu">
    <li><a href="{{ url('/') }}">Home</a></li>
    <li>
        <ul class="menu vertical submenu" data-submenu>
            <li class="title">Backups</li>
            <li><a href="{{ url('backup-management/list/all') }}">List</a></li>
        </ul>
    </li>
</ul>