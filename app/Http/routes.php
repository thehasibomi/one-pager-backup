<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    //return view('welcome');
    /*$output = new \Symfony\Component\Console\Output\BufferedOutput();
    \Illuminate\Support\Facades\Artisan::call('backup:run', array(), $output);
    echo '<pre>';
    print_r($output);*/

    /*\Illuminate\Support\Facades\Artisan::call('backup:run', array());
    $file = new App\Library\Backup();
    echo '<pre>';
    print_r(Artisan::output());
    print_r($file->getFileSize());*/

    return view('Home');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::group(['prefix' => 'backup-management/'], function() {
        Route::get('list/all', 'BackupManagementController@index');
        Route::get('view/{id}', 'BackupManagementController@show');
        Route::get('mark-as/deleted/{path}', 'BackupManagementController@delete');
        Route::get('mark-as/synced/{path}', 'BackupManagementController@synced');
    });

    Route::get('file', function() {
        $storage = Storage::disk(env('FILESYSTEM'));

        if($storage->exists('backups/20160220173711.zip'))
        {
            return "ok";
        }
        else
        {
            return "Not found";
        }
    });
});
