<?php

namespace App\Http\Controllers;

use App\BackupRun;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BackupManagementController extends Controller
{

    /**
     * List page of backup management.
     *
     * @return mixed
     */
    public function index()
    {
        return view('BackupManagement.All')->withBackups(BackupRun::all());
    }

    /**
     * Show a backup info.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $backup = BackupRun::getCompletedById($id);

        return view('BackupManagement.Show')->withBackup($backup);
    }

    /**
     * Sync backups by file path.
     *
     * @param $path
     * @return \Illuminate\Http\JsonResponse
     */
    public function synced($path)
    {
        BackupRun::syncedByFilepath($path);

        return response()->json(['message' => 'Success']);
    }

    /**
     * Delete backups by file path.
     *
     * @param $path
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($path)
    {
        BackupRun::deleteByFilepath($path);

        return response()->json(['message' => 'Success']);
    }

}
