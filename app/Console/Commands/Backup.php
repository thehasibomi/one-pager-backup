<?php

namespace App\Console\Commands;

use App\BackupRun;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Backup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:start {ondemand=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will run https://github.com/spatie/laravel-backup backup:run';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Backup process is started. Please wait until the process is finished.');

        global $file;

        $disk = \Storage::disk(env('FILESYSTEM'));
        $path = config('laravel-backup.destination.path');
        $allFiles = $disk->allFiles($path);

        if(count($allFiles) > 0)
        {
            $lastFile = count($allFiles) - 1;
            $file = $allFiles[$lastFile];
        }

        $create = BackupRun::create([
            'output_text' => 'Backup is started'
        ]);

        Artisan::call('backup:run', array());

        $update = BackupRun::findById($create->id);
        $update->path_to_backup = $file;
        $update->file_size = $disk->size($file);
        $update->output_text = Artisan::output();
        $update->is_completed = 1;
        $update->save();

        $this->info('Backup process is completed & saved to database.');
    }
}
