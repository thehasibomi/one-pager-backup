<?php

use Illuminate\Database\Seeder;

class BackupRunTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\BackupRun::create([
            'path_to_backup' => 'backups/20160220173711.zip',
            'file_size' => \Storage::disk(env('FILESYSTEM'))->size('backups/20160220173711.zip'),
            'is_completed' => 1
        ]);
    }
}
